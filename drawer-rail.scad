// clang-format off
include <utils/utils.scad>;
// clang-format on

/* [Precision] */
$fs = $preview ? 2 : 0.5;
$fa = $preview ? 2 : 0.5;
epsilon = 0.1;

/* [Rail] */
// Original design was 190mm
rail_length = 180;
rail_depth = 10;
rail_height = 10;
rail_edge_radius = 1;

/* [Wheels] */
wheels = true;
wheels_count = 5;
wheel_diameter = 14;
wheel_clearance = 2;
wheel_thickness = 5;
wheel_shaft_inner_diameter = 4;
wheel_shaft_outer_diameter = 7;
wheel_shaft_clearance = 1;
wheel_positions =
  [for (i = [0:wheels_count - 1]) wheel_diameter / 2 + epsilon +
    i / (wheels_count - 1) * (rail_length - wheel_diameter - epsilon)];

/* [Pins] */
pins = true;
pin_positions = [ 13, 108, 173 ];
pin_length = 8;
pin_diameters = [ 8, 8.5, 6 ];
pin_tip_length = 2;
pin_holes = false;
pin_hole_diameter = 3;

/* [Slot] */
slot = false;
slot_width = 6;
slot_length = 165;
slot_width_front = 11;
slot_length_front = 25;
slot_depth = 6;

/* [Display] */
slice_in_preview = false;

module rounded_cube(size, radius = -1, center = false)
{
  radius = radius < 0 ? min(size) * 0.25 : max(epsilon, radius);
  translate(center ? [ 0, 0, 0 ] : [ radius, radius ]) hull()
  {
    for (i = [ -1, 1 ]) {
      for (j = [ -1, 1 ]) {
        translate([
          i * (size[0] - 2 * radius) / 2 +
            (center ? 0 : (size[0] - 2 * radius) / 2),
          j * (size[1] - 2 * radius) / 2 +
            (center ? 0 : (size[1] - 2 * radius) / 2),
          center ? -size[2] / 2 : 0
        ]) cylinder(r = radius, h = size[2]);
      }
    }
  }
}

module shaft(inner_diameter = wheel_shaft_inner_diameter,
             outer_diameter = wheel_shaft_outer_diameter,
             extra_length = 0)
{
  chained_hull()
  {
    d = 0.25;
    translate([ 0, 0, epsilon / 2 - extra_length / 2 ])
      cylinder(d = outer_diameter, h = epsilon, center = true);
    translate([ 0, 0, wheel_clearance ])
      cylinder(d = outer_diameter, h = epsilon, center = true);
    translate([ 0, 0, wheel_clearance + wheel_thickness * d ])
      cylinder(d = inner_diameter, h = epsilon, center = true);
    translate([ 0, 0, wheel_clearance + wheel_thickness * (1 - d) ])
      cylinder(d = inner_diameter, h = epsilon, center = true);
    translate([
      0,
      0,
      wheel_clearance + wheel_thickness - epsilon / 2 + extra_length / 2
    ]) cylinder(d = outer_diameter, h = epsilon, center = true);
  }
}

render() intersection()
{

  if (slice_in_preview && $preview) {
    translate([ 0, 0, -(pin_length + epsilon / 2) ]) cube([
      rail_length + epsilon,
      rail_height / 2 + wheel_diameter / 2 + epsilon,
      pin_length + rail_depth +
      epsilon
    ]);
  }

  union()
  {
    render() difference()
    {
      union()
      {
        // the base shape
        translate([ 0, -rail_height / 2, 0 ]) rounded_cube(
          [ rail_length, rail_height, rail_depth ], radius = rail_edge_radius);

        if (pins) {
          at(pin_positions)
          {
            translate([ 0, 0, -(pin_length - pin_tip_length) ])
              cylinder(d1 = pin_diameters[1],
                       d2 = pin_diameters[0],
                       h = pin_length - pin_tip_length);
            translate([ 0, 0, -(pin_length) ]) cylinder(
              d1 = pin_diameters[2], d2 = pin_diameters[1], h = pin_tip_length);
          }
        }
      }

      // weird, rather unused slot in the original design
      if (slot) {
        chained_hull()
        {
          // front opening
          translate([ 0, 0, rail_depth - slot_depth ])
            cylinder(h = slot_depth + epsilon, d = slot_width_front);
          translate([ slot_length_front, 0, rail_depth - slot_depth ])
            cylinder(h = slot_depth, d = slot_width);
          translate([ slot_length, 0, rail_depth - slot_depth ])
            cylinder(h = slot_depth, d = slot_width);
        }
      }

      // clearance for the wheels
      if (wheels) {
        at(wheel_positions)
        {
          translate([ 0, 0, rail_depth - wheel_clearance - wheel_thickness ])
            cylinder(d = wheel_diameter + 2 * wheel_clearance,
                     h = wheel_thickness + wheel_clearance + epsilon);
        }
      }

      // weird holes through the pins in the original design
      if (pin_holes) {
        at(pin_positions)
        {
          translate([ 0, 0, -pin_length - epsilon / 2 ]) cylinder(
            d = pin_hole_diameter, h = rail_depth + pin_length + epsilon);
        }
      }
    }

    if (wheels) {
      // the wheels
      at(wheel_positions)
      {
        render() difference()
        {
          // the wheels
          translate([ 0, 0, rail_depth - wheel_thickness ]) difference()
            cylinder(d = wheel_diameter, h = wheel_thickness);

          // clearance for the shaft
          translate([ 0, 0, rail_depth - wheel_thickness - wheel_clearance ])
            shaft(inner_diameter =
                    wheel_shaft_inner_diameter + wheel_shaft_clearance * 2,
                  outer_diameter =
                    wheel_shaft_outer_diameter + 2 * wheel_shaft_clearance,
                  extra_length = epsilon);
        }
        // the shafts
        translate([ 0, 0, rail_depth - wheel_thickness - wheel_clearance ])
          shaft();
      }
    }
  }
}
